# JSON API For Discuz

#### 介绍
JSON API for Discuz！定义了一种接口鉴权、接口请求、接口返回、接口数据组织规范，给需要二次开发接口或定制接口的站长使用！希望有开发能力的站长可以一起升级维护！
应用中心主页：[https://addon.discuz.com/?@jsonapi.plugin](https://addon.discuz.com/?@jsonapi.plugin)

#### 插件说明

1、本插件是提供给有一定的接口开发需求和开发能力的站长使用的，普通站长安装本插件无实际作用；
2、本插件定义了一套接口规范，并定义了threadlist(主题列表)和userlist(用户列表)两个接口作为示例；
3、接口的开启关闭，以及接口字段是否返回，是否可排序，是否可筛选均可在后台接口设置中定义；
4、后台可自助添加应用及其接口授权；
5、为配合二次开发需求，本插件无语言包封装，所有提示文字部分均可在源码中修改，请务必注意修改后文件的编码方式；
6、插件会根据接口定义自动生成相应的接口帮助文档，帮助文档详见：http://你的域名/plugin.php?id=jsonapi:help

#### 注意TOKEN和appkey方式区别

1、TOKEN可请求全部开启的接口，用于接口测试；appkey方式只能请求授权给此appid的接口，正式场景中使用;
2、TOKEN方式示例：http://域名/plugin.php?id=jsonapi&token=test&api=threadlist&s_fid=34&s_tid=500&f_tid=gt&orderby=tid&sort=desc&page=1
2、appkey方式示例：http://域名/plugin.php?id=jsonapi&appid=xxx&appkey=yyy&api=threadlist&s_fid=34&s_tid=500&f_tid=gt&orderby=tid&sort=desc&page=1

#### 自定义接口开发说明

1、自定义接口开发参考接口：\source\plugin\jsonapi\api\jsonapi_threadlist.class.php 相关注释和说明；
2、开发能力说明，使用JSON API插件开发自定义拓展接口，需要一年以上PHP开发经验和半年以上Discuz!开发经验，需要基本熟悉discuz!数据表结构和插件开发规范！
3、JSON API接口开发核心思想	
3.1、API接口相互独立，限单表操作，固定字段集合；
3.2、接口使用模块化设计，增加和减少接口仅改变相应的接口类，不涉及插件其他文件；
3.3、按应用授权接口，权限独立；

#### 新增接口说明

1.接口文件放置于\source\plugin\jsonapi\api\目录下，文件名格式为：类名.class.php，其中类名为：jsonapi_api名称，必须严格按要求定义，否则不能被识别；
2.类的定义以 jsonapi_threadlist 为模板参考，实现重点：
2.1、静态定义api名称、介绍、字段集合等数据；
2.2、类实例化时加载缓存的配置信息，配置信息主要包括接口开启状态、接口返回字段、筛选字段、排序字段等内容，按本例格式处理即可；
2.3、实现接口数据返回方法 getData
3.由于涉及汉字，接口文件编码必须与当前网站编码方式一致，否则前后台调用的时候容易出现乱码！
4.接口调用方法详见：http://你的域名/plugin.php?id=jsonapi:help