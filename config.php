<?php
/*
 * 应用中心主页：http://addon.discuz.com/?@ailab
 * 人工智能实验室：Discuz!应用中心十大优秀开发者！
 * 插件定制 联系QQ594941227
 * From www.ailab.cn
 */
 
if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

//插件静态数据，如非必要情况，请勿修改！

$_JSONAPI=array(
	'wherefilter'=>array(
		'gt'=>'>',
		'lt'=>'<',
		'gte'=>'>=',
		'lte'=>'<=',
		'ne'=>'!=',
		'like'=>'like',
	),
	'sort'=>array(
		'desc',
		'asc'
	),
	'statuscode'=>array(
		-100=>'非法访问',
		-99=>'TOKEN错误',
		-98=>'没有对应API',
		-97=>'appkey为空',
		-96=>'appid错误',
		-95=>'appkey错误',
		-94=>'该API未得到授权',
		-93=>'该API未启用',
		
		-89=>'该API未设置返回数据',
		-88=>'超过最大可请求页数',
		
		100=>'ok',
	),
);


?>