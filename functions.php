<?php
/*
 * 应用中心主页：http://addon.discuz.com/?@ailab
 * 人工智能实验室：Discuz!应用中心十大优秀开发者！
 * 插件定制 联系QQ594941227
 * From www.ailab.cn
 */
 
if(!defined('IN_DISCUZ')) {
	exit('Access Denied');
}

$_JSONAPI=array();
require_once DISCUZ_ROOT.'./source/plugin/jsonapi/config.php';

function jsonApiLoad(){
	global $_JSONAPI;
	$filepath=DISCUZ_ROOT.'./source/plugin/jsonapi/api/';
	$handle=opendir($filepath); 
	while(false!==($file=readdir($handle))){ 
		if(substr_count($file,'.class.php')){
			$_JSONAPI['class'][]=str_replace('.class.php','',$file);
			require_once $filepath.$file;
		}
	}
}

function showApiError($code){
	echo json_encode(array('status'=>$code));
	exit;
}

function arrayToUtf8($arr){
	if(strtolower(CHARSET)=='utf-8') return $arr;
	foreach($arr as $k=>$v){
		if(is_array($v)){
			$arr[$k]=arrayToUtf8($v);
		}else{
			$arr[$k]=iconv('GB2312','UTF-8//IGNORE',$v);
		}
	}
	return $arr;
}


?>